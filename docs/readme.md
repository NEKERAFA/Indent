# Indent
Indent is a multi-paradigm scripting language focused in two fundamental things:

* Creates human-readable code with the same style.

* Extends easy the lenguage.

With this two premises, Indent is a small, lightweight and clearly language. The
current implementation of Indent Virtual Machine (Indent VM or IVM) is written
in ANSI C++, and is an hibrid machine, that combines stack-based machine and
register-based machine, doing more powerfull and small the implementation.

## Documentation

### C++ API

### Indent VM

#### 1. [Introduction](vm/readme.md#ivm)

#### 2. [Instruction](vm/readme.md#inst)
* [`NOP`](vm/readme.md#NOP)
* [`HALT`](vm/readme.md#HALT)
* [`LOADC $R #V`](vm/readme.md#LOADC)
* [`LOADI $R #V`](vm/readme.md#LOADI)
* [`LOADF $R #V`](vm/readme.md#LOADF)
* [`POP $R`](vm/readme.md#POP)
* [`POPI $R1 $R2`](vm/readme.md#POPI)
* [`PUSH $R`](vm/readme.md#PUSH)
* [`PUSHI $R1 $R2`](vm/readme.md#PUSHI)
* [`PUSHSTR $R`](vm/readme.md#PUSHSTR)
* [`MOV $R1 $R2`](vm/readme.md#MOV)
* [`ADD $R1 $R2`](vm/readme.md#ADD)
* [`SUB $R1 $R2`](vm/readme.md#SUB)
* [`MUL $R1 $R2`](vm/readme.md#MUL)
* [`DIV $R1 $R2`](vm/readme.md#DIV)
* [`MOD $R1 $R2`](vm/readme.md#MOD)
* [`NOT $R1`](vm/readme.md#NOT)
* [`AND $R1 $R2`](vm/readme.md#ADD)
* [`OR $R1 $R2`](vm/readme.md#OR)
* [`XOR $R1 $R2`](vm/readme.md#XOR)
* [`LSHIFT $R1 $R2`](vm/readme.md#LSHIFT)
* [`RSHIFT $R1 $R2`](vm/readme.md#RSHIFT)
* [`EQ $R1 $R2`](vm/readme.md#EQ)
* [`NE $R1 $R2`](vm/readme.md#NE)
* [`LT $R1 $R2`](vm/readme.md#LT)
* [`LE $R1 $R2`](vm/readme.md#LE)
* [`EQF $F1 $F2 #V`](vm/readme.md#EQF)
* [`NEF $F1 $F2 #V`](vm/readme.md#NEF)
* [`LTF $F1 $F2 #V`](vm/readme.md#LTF)
* [`LEF $F1 $F2 #V`](vm/readme.md#LEF)
* [`FOR R1 R2 R3`](vm/readme.md#FOR)
* [`FOREACH R1`](vm/readme.md#FOREACH)
* [`WHILE R1`](vm/readme.md#WHILE)
* [`ENDLOOP`](vm/readme.md#ENDLOOP)
* [`JUMP #V`](vm/readme.md#JUMP)
* [`NEWTABLE`](vm/readme.md#NEWTABLE)
* [`GETTABLE $R1 $R2`](vm/readme.md#GETTABLE)
* [`SETTABLE $R1 $R2`](vm/readme.md#SETTABLE)
* [`NEWLIST`](vm/readme.md#NEWLIST)
* [`GETLIST $R`](vm/readme.md#GETLIST)
* [`SETLIST $R`](vm/readme.md#SETLIST)
* [`TAILLIST $R`](vm/readme.md#TAILLIST)
* [`NEWSET`](vm/readme.md#NEWSET)
* [`ADDSET $R`](vm/readme.md#ADDSET)
* [`TESTSET $R`](vm/readme.md#TESTSET)
* [`NEW $R`](vm/readme.md#NEW)
* [`SELF $R1 $R2 $R3`](vm/readme.md#SELF)
* [`CALL $R1 $R2`](vm/readme.md#CALL)

### Indent Language
