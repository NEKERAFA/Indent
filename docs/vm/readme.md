[Indent Virtual Machine](#ivm)
===

The current implementation of IVM are written in C++. Is a register-based machine with the next architecture:

* The Virtual Machine have 256 records that point to a type value.
* The instructions have 1 byte of code (`uint8_t`), and each has variable size.
The size results from the current instruction read.

[Instructions](#inst)
---

| Instruction | Code | Size | Description |
| --- | :---: | :---: | --- |
| [`NOP`](#NOP) | 0x00 | 1 B | Does nothing |
| [`HALT`](#HALT) | 0xFF | 1 B | Halts the machine, resulting of the finalisation of current computation |
| [`LOADC $R #V`](#LOADC) | 0x01 | 3 B | `R := #V : chr` |
| [`LOADI $R #V`](#LOADI) | 0x02 | 6 B | `R := #V : int` |
| [`LOADF $R #V`](#LOADF) | 0x03 | 6 B* | `F := #V : float` |
| [`POP $R`](#POP) | 0x04 | 2 B | `R := STACK.POP` |
| [`POPI $R1 $R2`](#POPI) | 0x05 | 2 B | `R1 := STACK.POP(R2 : int)` |
| [`PUSH $R`](#PUSH) | 0x06 | 2 B | `STACK.PUSH(R)` |
| [`PUSHI $R1 $R2`](#PUSHI) | 0x07 | 2 B | `STACK.PUSH(R1, R2: int)` |
| [`PUSHSTR $R`](#PUSHSTR) | 0x08 | 2 B | ` S1 : str or char := STACK.POP; S2 := (R : chr) ∩ S1; STRING.PUSH(S2)` |
| [`MOV $R1 $R2`](#MOV) | 0x09 | 3 B | `R2 := R1` |
| [`ADD $R1 $R2`](#ADD) | 0x0A | 3 B | `R1 := R1 + R2` |
| [`SUB $R1 $R2`](#SUB) | 0x0B | 3 B | `R1 := R1 - R2` |
| [`MUL $R1 $R2`](#MUL) | 0x0C | 3 B | `R1 := R1 * R2` |
| [`DIV $R1 $R2`](#DIV) | 0x0D | 3 B | `R1 := R1 / R2` |
| [`MOD $R1 $R2`](#MOD) | 0x0E | 3 B | `R1 := R1 MOD R2` |
| [`NOT $R1`](#NOT) | 0x0F | 2 B | `R1 := ¬ R1` |
| [`AND $R1 $R2`](#ADD) | 0x10 | 3 B | `R1 := R1 ∧ R2` |
| [`OR $R1 $R2`](#OR) | 0x11 | 3 B | `R1 := R1 ∨ R2` |
| [`XOR $R1 $R2`](#XOR) | 0x12 | 3 B | `R1 := R1 ⊻ R2` |
| [`LSHIFT $R1 $R2`](#LSHIFT) | 0x13 | 3 B | `R1 := R1 << R2` |
| [`RSHIFT $R1 $R2`](#RSHIFT) | 0x14 | 3 B | `R1 := R1 >> R2` |
| [`EQ $R1 $R2`](#EQ) | 0x15 | 3 B | Compares `R1 = R2` and jumps IR+2, otherwise jumps IR+1 |
| [`NE $R1 $R2`](#NE) | 0x16 | 3 B | Compares `R1 ≠ R2` and jumps IR+2, otherwise jumps IR+1 |
| [`LT $R1 $R2`](#LT) | 0x17 | 3 B | Compares `R1 < R2` and jumps IR+2, otherwise jumps IR+1 |
| [`LE $R1 $R2`](#LE) | 0x18 | 3 B | Compares `R1 ≤ R2` and jumps IR+2, otherwise jumps IR+1 |
| [`EQF $F1 $F2 #V`](#EQF) | 0x19 | 7 B* | `EQ` for floats using a epsilon |
| [`NEF $F1 $F2 #V`](#NEF) | 0x1A | 7 B* | `NE` for floats using a epsilon |
| [`LTF $F1 $F2 #V`](#LTF) | 0x1B | 7 B* | `LT` for floats using a epsilon |
| [`LEF $F1 $F2 #V`](#LEF) | 0x1C | 7 B* | `LE` for floats using a epsilon |
| [`FOR R1 R2 R3`](#FOR) | 0x1D | 4 B | Executes a `FOR R1 TO R2 STEP R3` |
| [`FOREACH R1`](#FOREACH) | 0x1E | 2 B | Executes a `V := STACK.POP; FOR R1 IN V` (`V` must be a table, a list or a set) |
| [`WHILE R1`](#WHILE) | 0x1F | 2 B | Executes a `WHILE R1 ≠ 0` |
| [`ENDLOOP`](#ENDLOOP) | 0x20 | 1 B | Returns to last loop operation or do IR+1 if the loop is finished |
| [`JUMP #V`](#JUMP) | 0x021 | 5 B | Jumps using `IR := IR + #V` |
| [`NEWTABLE`](#NEWTABLE) | 0x22 | 1 B | Creates new table in the stack |
| [`GETTABLE $R1 $R2`](#GETTABLE) | 0x23 | 3 B | `T : table := STACK.POP; R1 := T[R2]` |
| [`SETTABLE $R1 $R2`](#SETTABLE) | 0x24 | 3 B | `T : table := STACK.POP; T[R2] := R1; STACK.PUSH(T)` |
| [`NEWLIST`](#NEWLIST) | 0x25 | 1 B | Creates new list in the stack |
| [`GETLIST $R`](#GETLIST) | 0x26 | 2 B | `L : list := STACK.POP; R1 := L.HEAD ` |
| [`SETLIST $R`](#SETLIST) | 0x27 | 2 B | `L : list := STACK.POP; L.HEAD := R1; STACK.PUSH(L)` |
| [`TAILLIST $R`](#TAILLIST) | 0x28 | 2 B | `L : list := STACK.POP; R1 := L.TAIL` |
| [`NEWSET`](#NEWSET) | 0x29 | 1 B | Creates new set in the stack |
| [`ADDSET $R`](#ADDSET) | 0x2A | 2 B | `S : set := STACK.POP; {R1} ∩ SET; STACK.PUSH(S)` |
| [`TESTSET $R`](#TESTSET) | 0x2B | 2 B | `S : set := STACK.POP`, checks if `R ∊ SET` and  jumps IR+2, otherwise jumps IR+1 |
| [`NEW $R`](#NEW) | 0x2C | 2 B | Creates new object of `R : string` name class in the stack |
| [`SELF $R1 $R2 $R3`](#SELF) | 0x2D | 3 B | `R3 := (R2 : string => R1 : object (STACK))` |
| [`CALL $R1 $R2`](#CALL) | 0x2E | 3 B | `R2, STACK := (R1 : function)(STACK)` |

\* For floating-point numbers, size is aproximatly. It depends of the C++
compiler implementation and the system architecture.
