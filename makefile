# File: makefile.c 23.12.2017 12:39:21 nekerafa
#
# See Indent.h for Copyright Notice

CC    = g++
CFLAG = -std=c++11 -Wall
OBJS  = indent_vm.o indent_ops.o

all: compile
	./indent

%.o: src/%.cc
	$(CC) -c $(CFLAG) $(CLIBS) $<

compile: $(OBJS)
	$(CC) $(CFLAG) $(CLIBS) -o indent src/indent.cc $(OBJS)

dbg: $(OBJS)
	$(CC) -g $(CFLAG) $(CLIBS) -o indent src/indent.cc $(OBJS)

clean:
	rm -rfv *.o indent
