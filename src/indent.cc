#include <iostream>
#include <cstring>
#include "indent_vm.h"

int main(int argc, char const *argv[]) {
  indent::InitVMOps();

  float pi = 3.14;
  float arc = 1.2;

  indent::byte_t instr[24] = {};

  instr[0] = 0x01; // LOADR R0 #3.14
  instr[1] = 0x00;
  memcpy(instr+2, &pi, sizeof(indent::word_t));
  instr[6] = 0x01; // LOADR R1 #1.2
  instr[7] = 0x01;
  memcpy(instr+8, &arc, sizeof(indent::word_t));
  instr[12] = 0x0A; // ADDF R1 R2 R3
  instr[13] = 0x00;
  instr[14] = 0x01;
  instr[15] = 0x02;
  instr[23] = 0xFF; // HALT

  std::vector<indent::byte_t> program (instr, instr+24);

  indent::Machine vm (program, 64);
  vm.Run();

  return 0;
}
