#include <iostream>
#include <iomanip>
#include <cstring>

#include "indent_vm.h"
#include "indent_ops.h"

// VM Operation set

namespace indent {

  // NOP Instruction
  dword_t nop(vm_info_t& info) {
    std::cout << "NOP" << std::endl;
    return info.ir+0x01;
  }

  // HALT Instruction
  dword_t halt(vm_info_t& info) {
    std::cout << "HALT" << std::endl;
    info.int_r |= INDENT_VM_HALT;
    return info.ir;
  }

  // Load to register
  dword_t loadr(vm_info_t& info) {
    if ((info.ir + 6) >= info.instr_size_r) {
      info.int_r |= INDENT_VM_IOF;
      return info.ir;
    }

    byte_t* data = info.instr.data();
    byte_t reg_n = *(data + info.ir + 1);

    memcpy(&info.regs[reg_n], (data + info.ir + 2), sizeof(word_t));

    std::cout << "LOADR R" << (unsigned) reg_n << " #" << info.regs[reg_n] << std::endl;
    return info.ir + 6;
  }

  // Load from stack
  dword_t load(vm_info_t& info) {
    if (info.sp == 0) {
      info.int_r |= INDENT_VM_EMPT;
      return info.ir;
    }

    if ((info.ir + 2) >= info.instr_size_r) {
      info.int_r |= INDENT_VM_IOF;
      return info.ir;
    }

    info.sp--;
    byte_t reg_n = info.instr[info.ir+0x01];
    info.regs[reg_n] = info.memory.back();
    info.memory.pop_back();

    std::cout << "LOAD R" << (unsigned) reg_n << "\t; #" << info.regs[reg_n] << std::endl;
    return info.ir+0x02;
  }

  // Store a value in the stack
  dword_t store(vm_info_t& info) {
    if (info.sp == info.mem_size_r) {
      info.int_r |= INDENT_VM_MOF;
      return info.ir;
    }

    byte_t reg_n = info.instr[info.ir+0x01];
    info.memory.push_back(info.regs[reg_n]);
    info.sp++;

    std::cout << "STORE R" << (unsigned) reg_n << "\t ; #" << info.regs[reg_n];
    std::cout << ", SP: 0x" << std::setfill('0') << std::setw(32) << std::hex;
    std::cout << info.sp << std::dec << std::endl;
    return info.ir+0x02;
  }

  // Move a value between registers
  dword_t move(vm_info_t& info) {
    byte_t reg_a = info.instr[info.ir+0x01];
    byte_t reg_b = info.instr[info.ir+0x02];
    info.regs[reg_b] = info.regs[reg_a];

    std::cout << "MOVE R" << (unsigned) reg_a << " R" << (unsigned) reg_b;
    std::cout << "\t; #" << info.regs[reg_b] << std::endl;
    return info.ir+0x03;
  }

  // Add two registers and save
  dword_t add(vm_info_t& info) {
    byte_t reg_a = info.instr[info.ir+0x01];
    byte_t reg_b = info.instr[info.ir+0x02];
    byte_t reg_c = info.instr[info.ir+0x03];

    info.regs[reg_c] = info.regs[reg_a] + info.regs[reg_b];

    std::cout << "ADD R" << (unsigned) reg_a << " R" << (unsigned) reg_b;
    std::cout << " R" << (unsigned) reg_c << "\t; #" << info.regs[reg_c] << std::endl;
    return info.ir+0x04;
  }

  // Substract two registers and save
  dword_t sub(vm_info_t& info) {
    byte_t reg_a = info.instr[info.ir+0x01];
    byte_t reg_b = info.instr[info.ir+0x02];
    byte_t reg_c = info.instr[info.ir+0x03];

    info.regs[reg_c] = info.regs[reg_a] - info.regs[reg_b];

    std::cout << "SUB R" << (unsigned) reg_a << " R" << (unsigned) reg_b;
    std::cout << " R" << (unsigned) reg_c << "\t; #" << info.regs[reg_c] << std::endl;
    return info.ir+0x04;
  }

  // Multiplies two registers and save
  dword_t mul(vm_info_t& info) {
    byte_t reg_a = info.instr[info.ir+0x01];
    byte_t reg_b = info.instr[info.ir+0x02];
    byte_t reg_c = info.instr[info.ir+0x03];

    info.regs[reg_c] = info.regs[reg_a] * info.regs[reg_b];

    std::cout << "MUL R" << (unsigned) reg_a << " R" << (unsigned) reg_b;
    std::cout << " R" << (unsigned) reg_c << "\t; #" << info.regs[reg_c] << std::endl;
    return info.ir+0x04;
  }

  // Divides two registers and save
  dword_t div(vm_info_t& info) {
    byte_t reg_a = info.instr[info.ir+0x01];
    byte_t reg_b = info.instr[info.ir+0x02];
    byte_t reg_c = info.instr[info.ir+0x03];

    info.regs[reg_c] = info.regs[reg_a] / info.regs[reg_b];

    std::cout << "DIV R" << (unsigned) reg_a << " R" << (unsigned) reg_b;
    std::cout << " R" << (unsigned) reg_c << "\t; #" << info.regs[reg_c] << std::endl;
    return info.ir+0x04;
  }

  // Computes a module between two registers and save
  dword_t mod(vm_info_t& info) {
    byte_t reg_a = info.instr[info.ir+0x01];
    byte_t reg_b = info.instr[info.ir+0x02];
    byte_t reg_c = info.instr[info.ir+0x03];

    info.regs[reg_c] = info.regs[reg_a] % info.regs[reg_b];

    std::cout << "MOD R" << (unsigned) reg_a << " R" << (unsigned) reg_b;
    std::cout << " R" << (unsigned) reg_c << "\t; #" << info.regs[reg_c] << std::endl;
    return info.ir+0x04;
  }

  // Add two registers and save
  dword_t addf(vm_info_t& info) {
    float value_a = 0.0;
    float value_b = 0.0;
    float value_c = 0.0;

    byte_t reg_a = info.instr[info.ir+0x01];
    byte_t reg_b = info.instr[info.ir+0x02];
    byte_t reg_c = info.instr[info.ir+0x03];

    word_t word_a = info.regs[reg_a];
    word_t word_b = info.regs[reg_b];
    word_t word_c = INDENT_VM_ZEROWORD;

    memcpy(&value_a, &word_a, sizeof(float));
    memcpy(&value_b, &word_b, sizeof(float));

    value_c = value_a + value_b;

    memcpy(&word_c, &value_c, sizeof(float));

    info.regs[reg_c] = word_c;

    std::cout << "ADDF R" << (unsigned) reg_a << " R" << (unsigned) reg_b;
    std::cout << " R" << (unsigned) reg_c << "\t; #" << value_a << ", #";
    std::cout << value_b << ", #" << value_c << std::endl;
    return info.ir+0x04;
  }

}
