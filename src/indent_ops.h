#ifndef INDENT_VMOPS_H
#define INDENT_VMOPS_H 1

#include "indent_vm.h"

namespace indent {
  // NOP Instruction
  op_t nop;
  // HALT Instruction
  op_t halt;
  // LOADR Instruction
  op_t loadr;
  // LOAD Instruction
  op_t load;
  // STORE Instruction
  op_t store;
  // MOVE Instruction
  op_t move;
  // ADD Instruction
  op_t add;
  // SUB Instruction
  op_t sub;
  // MUL Instruction
  op_t mul;
  // DIV Instruction
  op_t div;
  // MOD Instruction
  op_t mod;
  // ADDF Instruction
  op_t addf;
  // SUBF Instruction
  op_t subf;
  // MULF Instruction
  op_t mulf;
  // DIVF Instruction
  op_t divf;
}

#endif
