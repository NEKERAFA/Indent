#include <vector>
#include <array>
#include <cstdint>
#include <iostream>
#include <iomanip>

#include "indent_vm.h"
#include "indent_ops.h"

namespace indent {

  // Operation array
  op_t* ops[256];

  void InitVMOps() {
    for (int i = 0; i < 256; i++) {
      ops[i] = &nop;
    }

    ops[0x01] = &loadr;
    ops[0x02] = &load;
    ops[0x03] = &store;
    ops[0x04] = &move;
    ops[0x05] = &add;
    ops[0x06] = &sub;
    ops[0x07] = &mul;
    ops[0x08] = &div;
    ops[0x09] = &mod;
    ops[0x0A] = &addf;
    ops[0xFF] = &halt;
  }

  // Decodes a instruction and execute their function
  void Machine::Decode(byte_t instr) {
    word_t jump = ops[instr](info_);

    if (jump > info_.instr_size_r) {
      info_.int_r |= INDENT_VM_IOF;
      return;
    }

    info_.ir = jump;
  }

  // Executes new cycle in the machine
  void Machine::NewCycle() {
    Decode(info_.instr[info_.ir]);
    info_.cr++;
  }

  // Starts the machine and prints hello message
  void Machine::Start() {
    std::cout << "Indent VM: " << info_.mem_size_r * sizeof(dword_t) << "B of memory ";
    std::cout << "allocated. " << info_.instr_size_r * sizeof(byte_t) << "B of instructions ";
    std::cout << "allocated." << std::endl;
  }

  // Ends the machine when a interruption appears and prints a resume of execution
  void Machine::EndInfo() {
    std::cout << "> Interruption: ";

    switch (info_.int_r) {
      case INDENT_VM_HALT:
        std::cout << "HALT: " << info_.cr << " cycles do. ";
        std::cout << "Finish in 0x" << std::setfill('0') << std::setw(16);
        std::cout << std::hex << info_.ir << std::dec << std::endl;
        break;

      case INDENT_VM_MOF:
        std::cout << "MEMORY OVERFLOW: Maximun=" << info_.mem_size_r << ", Reached=";
        std::cout << info_.sp << ". Instruction 0x" << std::setfill('0');
        std::cout << std::setw(16) << std::hex << info_.ir << std::dec << std::endl;
        break;

      case INDENT_VM_EMPT:
        std::cout << "MEMORY STACK EMPTY: Maximun=" << info_.mem_size_r << " Reached=";
        std::cout << info_.sp << ". Instruction 0x" << std::setfill('0');
        std::cout << std::setw(16) << std::hex << info_.ir << std::dec << std::endl;
        break;

      case INDENT_VM_IOF:
        std::cout << "INSTRUCTION OVERFLOW: Maximun=" << info_.instr_size_r << ". ";
        std::cout << "Instruction 0x" << std::setfill('0') << std::setw(16);
        std::cout << std::hex << info_.ir << std::endl;
        break;
    }
  }

  // Creates new machine passing a program and the maximun size of stack
  Machine::Machine(std::vector<byte_t> prog, dword_t size) {
    info_.memory = std::vector<word_t>(size);
    info_.mem_size_r = size;
    info_.instr = prog;
    info_.instr_size_r = prog.size();
    info_.ir = INDENT_VM_ZERODWORD;
    info_.sp = INDENT_VM_ZERODWORD;
  }

  // Creates new machine passing a program, the maximun size of stack and a stack
  Machine::Machine(std::vector<byte_t> prog, dword_t size, std::vector<word_t> memory) {
    info_.memory = memory;
    info_.mem_size_r = size;
    info_.instr = prog;
    info_.instr_size_r = prog.size();
    info_.ir = INDENT_VM_ZERODWORD;
    info_.sp = memory.size();
  }

  // Runs the machine
  void Machine::Run() {
    info_.cr = INDENT_VM_ZEROWORD;
    info_.int_r = INDENT_VM_RUN;

    Start();
    while(info_.int_r == 0x00) {
      NewCycle();
    }
    EndInfo();
  }

  // Load new program deleting stack container and putting all registers to zero
  void Machine::Load(std::vector<byte_t> prog) {
    info_.instr = prog;
    info_.instr_size_r = prog.size();
    info_.memory = std::vector<word_t>(info_.mem_size_r);
    info_.ir = INDENT_VM_ZERODWORD;
    info_.sp = INDENT_VM_ZERODWORD;

    for(int i = 0; i < 256; i++) {
      info_.regs[i] = INDENT_VM_ZEROWORD;
    }
  }

  // Load new program and new stack putting all registers to zero
  void Machine::Load(std::vector<byte_t> prog, dword_t size, std::vector<word_t> mem) {
    Load(prog);
    info_.memory = mem;
    info_.mem_size_r = size;
  }

  // Get the direcction of current instruction
  dword_t Machine::CurrentInstruction() {
    return info_.ir;
  }

  // Get the current cycles doing by the machine
  dword_t Machine::GetCycles() {
    return info_.cr;
  }

} /* indent */
