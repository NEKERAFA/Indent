#ifndef INDENT_VM_H
#define INDENT_VM_H 1

#include <vector>
#include <array>
#include <cstdint>

// Interrupt flags for VM
#define INDENT_VM_RUN  0x00
#define INDENT_VM_HALT 0x01
#define INDENT_VM_MOF  0x02
#define INDENT_VM_EMPT 0x04
#define INDENT_VM_IOF  0x08

// For set 0 bytes and avoid casts conversions
#define INDENT_VM_ZEROBYTE  0x00;
#define INDENT_VM_ZEROWORD  0x00000000;
#define INDENT_VM_ZERODWORD 0x0000000000000000;

namespace indent {

  // VM data Types
  typedef uint8_t  byte_t;
  typedef uint32_t word_t;
  typedef uint64_t dword_t;

  typedef struct {
    /*** Specific registers ***/
    // Instruction counter register
    dword_t ir;
    // Stack pointer
    dword_t sp;
    // Cycle counter register
    word_t cr;
    // Interuption register
    // 0: HALT/RUN
    // 1: MEMORY OVERFLOW
    // 2: EMPTY MEMORY
    // 3: INSTRUCTION COUNTER OVERFLOW
    // 5:
    // 6:
    // 7:
    byte_t int_r;
    // Memory stack size register
    dword_t mem_size_r;
    // Instruction vector size register
    dword_t instr_size_r;

    /*** General registers ***/
    word_t regs[256];

    /*** Memory and instruction vectors ***/
    std::vector<byte_t> instr;
    std::vector<word_t> memory;
  } vm_info_t;

  // Operation Type
  typedef dword_t (op_t)(vm_info_t&);

  // Initializes the operations array
  void InitVMOps();

  // This class emulates a machine
  class Machine {
  private:
    vm_info_t info_;

    // Decodes a instruction and execute their function
    void Decode(byte_t);
    // Executes new cycle in the machine
    void NewCycle();
    // Starts the machine and prints hello message
    void Start();
    // Ends the machine when a interruption appears and prints a resume of execution
    void EndInfo();
  public:
    // Creates new machine passing a program and the maximun size of stack
    Machine(std::vector<byte_t>, dword_t);
    // Creates new machine passing a program, the maximun size of stack and a stack
    Machine(std::vector<byte_t>, dword_t, std::vector<word_t>);
    // Runs the machine
    void Run();
    // Load new program deleting stack container and putting all registers to zero
    void Load(std::vector<byte_t>);
    // Load new program and new stack putting all registers to zero
    void Load(std::vector<byte_t>, dword_t, std::vector<word_t>);
    // Get the direcction of current instruction
    dword_t CurrentInstruction();
    // Get the current cycles doing by the machine
    dword_t GetCycles();
  };

} /* indent */

#endif
